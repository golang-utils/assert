//go:build production
// +build production

package assert

import (
	"net/http"
	"net/url"
	"time"

	"github.com/stretchr/testify/assert"
)

func Condition(comp assert.Comparison, msgAndArgs ...interface{}) bool {
	return true
}

func Conditionf(comp assert.Comparison, msg string, args ...interface{}) bool {
	return true
}

func Contains(s interface{}, contains interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func Containsf(s interface{}, contains interface{}, msg string, args ...interface{}) bool {
	return true
}

func DirExists(path string, msgAndArgs ...interface{}) bool {
	return true
}

func DirExistsf(path string, msg string, args ...interface{}) bool {
	return true
}

func ElementsMatch(listA interface{}, listB interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func ElementsMatchf(listA interface{}, listB interface{}, msg string, args ...interface{}) bool {
	return true
}

func Empty(object interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func Emptyf(object interface{}, msg string, args ...interface{}) bool {
	return true
}

func Equal(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func EqualError(theError error, errString string, msgAndArgs ...interface{}) bool {
	return true
}

func EqualErrorf(theError error, errString string, msg string, args ...interface{}) bool {
	return true
}

func EqualExportedValues(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func EqualExportedValuesf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	return true
}

func EqualValues(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func EqualValuesf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	return true
}

func Equalf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	return true
}

func Error(err error, msgAndArgs ...interface{}) bool {
	return true
}

func ErrorAs(err error, target interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func ErrorAsf(err error, target interface{}, msg string, args ...interface{}) bool {
	return true
}

func ErrorContains(theError error, contains string, msgAndArgs ...interface{}) bool {
	return true
}

func ErrorContainsf(theError error, contains string, msg string, args ...interface{}) bool {
	return true
}

func ErrorIs(err error, target error, msgAndArgs ...interface{}) bool {
	return true
}

func ErrorIsf(err error, target error, msg string, args ...interface{}) bool {
	return true
}

func Errorf(err error, msg string, args ...interface{}) bool {
	return true
}

func Eventually(condition func() bool, waitFor time.Duration, tick time.Duration, msgAndArgs ...interface{}) bool {
	return true
}

func EventuallyWithT(condition func(collect *assert.CollectT), waitFor time.Duration, tick time.Duration, msgAndArgs ...interface{}) bool {
	return true
}

func EventuallyWithTf(condition func(collect *assert.CollectT), waitFor time.Duration, tick time.Duration, msg string, args ...interface{}) bool {
	return true
}

func Eventuallyf(condition func() bool, waitFor time.Duration, tick time.Duration, msg string, args ...interface{}) bool {
	return true
}

func Exactly(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func Exactlyf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	return true
}

func Fail(failureMessage string, msgAndArgs ...interface{}) bool {
	return true
}

func FailNow(failureMessage string, msgAndArgs ...interface{}) bool {
	return true
}

func FailNowf(failureMessage string, msg string, args ...interface{}) bool {
	return true
}

func Failf(failureMessage string, msg string, args ...interface{}) bool {
	return true
}

func False(value bool, msgAndArgs ...interface{}) bool {
	return true
}

func Falsef(value bool, msg string, args ...interface{}) bool {
	return true
}

func FileExists(path string, msgAndArgs ...interface{}) bool {
	return true
}

func FileExistsf(path string, msg string, args ...interface{}) bool {
	return true
}

func Greater(e1 interface{}, e2 interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func GreaterOrEqualf(e1 interface{}, e2 interface{}, msg string, args ...interface{}) bool {
	return true
}

func Greaterf(e1 interface{}, e2 interface{}, msg string, args ...interface{}) bool {
	return true
}

func HTTPBodyContains(handler http.HandlerFunc, method string, url string, values url.Values, str interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func HTTPBodyContainsf(handler http.HandlerFunc, method string, url string, values url.Values, str interface{}, msg string, args ...interface{}) bool {
	return true
}

func HTTPBodyNotContains(handler http.HandlerFunc, method string, url string, values url.Values, str interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func HTTPBodyNotContainsf(handler http.HandlerFunc, method string, url string, values url.Values, str interface{}, msg string, args ...interface{}) bool {
	return true
}

func HTTPError(handler http.HandlerFunc, method string, url string, values url.Values, msgAndArgs ...interface{}) bool {
	return true
}

func HTTPErrorf(handler http.HandlerFunc, method string, url string, values url.Values, msg string, args ...interface{}) bool {
	return true
}

func HTTPRedirect(handler http.HandlerFunc, method string, url string, values url.Values, msgAndArgs ...interface{}) bool {
	return true
}

func HTTPRedirectf(handler http.HandlerFunc, method string, url string, values url.Values, msg string, args ...interface{}) bool {
	return true
}

func HTTPStatusCode(handler http.HandlerFunc, method string, url string, values url.Values, statuscode int, msgAndArgs ...interface{}) bool {
	return true
}

func HTTPStatusCodef(handler http.HandlerFunc, method string, url string, values url.Values, statuscode int, msg string, args ...interface{}) bool {
	return true
}

func HTTPSuccess(handler http.HandlerFunc, method string, url string, values url.Values, msgAndArgs ...interface{}) bool {
	return true
}

func HTTPSuccessf(handler http.HandlerFunc, method string, url string, values url.Values, msg string, args ...interface{}) bool {
	return true
}

func Implements(interfaceObject interface{}, object interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func Implementsf(interfaceObject interface{}, object interface{}, msg string, args ...interface{}) bool {
	return true
}

func InDelta(expected interface{}, actual interface{}, delta float64, msgAndArgs ...interface{}) bool {
	return true
}

func InDeltaMapValues(expected interface{}, actual interface{}, delta float64, msgAndArgs ...interface{}) bool {
	return true
}

func InDeltaMapValuesf(expected interface{}, actual interface{}, delta float64, msg string, args ...interface{}) bool {
	return true
}

func InDeltaSlice(expected interface{}, actual interface{}, delta float64, msgAndArgs ...interface{}) bool {
	return true
}

func InDeltaSlicef(expected interface{}, actual interface{}, delta float64, msg string, args ...interface{}) bool {
	return true
}

func InDeltaf(expected interface{}, actual interface{}, delta float64, msg string, args ...interface{}) bool {
	return true
}

func InEpsilon(expected interface{}, actual interface{}, epsilon float64, msgAndArgs ...interface{}) bool {
	return true
}

func InEpsilonSlice(expected interface{}, actual interface{}, epsilon float64, msgAndArgs ...interface{}) bool {
	return true
}

func InEpsilonSlicef(expected interface{}, actual interface{}, epsilon float64, msg string, args ...interface{}) bool {
	return true
}

func InEpsilonf(expected interface{}, actual interface{}, epsilon float64, msg string, args ...interface{}) bool {
	return true
}

func IsDecreasing(object interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func IsDecreasingf(object interface{}, msg string, args ...interface{}) bool {
	return true
}

func IsIncreasing(object interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func IsIncreasingf(object interface{}, msg string, args ...interface{}) bool {
	return true
}

func IsNonDecreasing(object interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func IsNonDecreasingf(object interface{}, msg string, args ...interface{}) bool {
	return true
}

func IsNonIncreasing(object interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func IsNonIncreasingf(object interface{}, msg string, args ...interface{}) bool {
	return true
}

func IsType(expectedType interface{}, object interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func IsTypef(expectedType interface{}, object interface{}, msg string, args ...interface{}) bool {
	return true
}

func JSONEq(expected string, actual string, msgAndArgs ...interface{}) bool {
	return true
}

func JSONEqf(expected string, actual string, msg string, args ...interface{}) bool {
	return true
}

func Len(object interface{}, length int, msgAndArgs ...interface{}) bool {
	return true
}

func Lenf(object interface{}, length int, msg string, args ...interface{}) bool {
	return true
}

func Less(e1 interface{}, e2 interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func LessOrEqual(e1 interface{}, e2 interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func LessOrEqualf(e1 interface{}, e2 interface{}, msg string, args ...interface{}) bool {
	return true
}

func Lessf(e1 interface{}, e2 interface{}, msg string, args ...interface{}) bool {
	return true
}

func Negative(e interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func Negativef(e interface{}, msg string, args ...interface{}) bool {
	return true
}

func Never(condition func() bool, waitFor time.Duration, tick time.Duration, msgAndArgs ...interface{}) bool {
	return true
}

func Neverf(condition func() bool, waitFor time.Duration, tick time.Duration, msg string, args ...interface{}) bool {
	return true
}

func Nil(object interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func Nilf(object interface{}, msg string, args ...interface{}) bool {
	return true
}

func NoDirExists(path string, msgAndArgs ...interface{}) bool {
	return true
}

func NoDirExistsf(path string, msg string, args ...interface{}) bool {
	return true
}

func NoError(err error, msgAndArgs ...interface{}) bool {
	return true
}

func NoErrorf(err error, msg string, args ...interface{}) bool {
	return true
}

func NoFileExists(path string, msgAndArgs ...interface{}) bool {
	return true
}

func NoFileExistsf(path string, msg string, args ...interface{}) bool {
	return true
}

func NotContains(s interface{}, contains interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func NotContainsf(s interface{}, contains interface{}, msg string, args ...interface{}) bool {
	return true
}

func NotEmpty(object interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func NotEmptyf(object interface{}, msg string, args ...interface{}) bool {
	return true
}

func NotEqual(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func NotEqualValues(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func NotEqualValuesf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	return true
}

func NotEqualf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	return true
}

func NotErrorIs(err error, target error, msgAndArgs ...interface{}) bool {
	return true
}

func NotErrorIsf(err error, target error, msg string, args ...interface{}) bool {
	return true
}

func NotNil(object interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func NotNilf(object interface{}, msg string, args ...interface{}) bool {
	return true
}

func NotPanics(f assert.PanicTestFunc, msgAndArgs ...interface{}) bool {
	return true
}

func NotPanicsf(f assert.PanicTestFunc, msg string, args ...interface{}) bool {
	return true
}

func NotRegexp(rx interface{}, str interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func NotRegexpf(rx interface{}, str interface{}, msg string, args ...interface{}) bool {
	return true
}

func NotSame(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func NotSamef(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	return true
}

func NotSubset(list interface{}, subset interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func NotSubsetf(list interface{}, subset interface{}, msg string, args ...interface{}) bool {
	return true
}

func NotZero(i interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func NotZerof(i interface{}, msg string, args ...interface{}) bool {
	return true
}

func Panics(f assert.PanicTestFunc, msgAndArgs ...interface{}) bool {
	return true
}

func PanicsWithError(errString string, f assert.PanicTestFunc, msgAndArgs ...interface{}) bool {
	return true
}

func PanicsWithErrorf(errString string, f assert.PanicTestFunc, msg string, args ...interface{}) bool {
	return true
}

func PanicsWithValue(expected interface{}, f assert.PanicTestFunc, msgAndArgs ...interface{}) bool {
	return true
}

func PanicsWithValuef(expected interface{}, f assert.PanicTestFunc, msg string, args ...interface{}) bool {
	return true
}

func Panicsf(f assert.PanicTestFunc, msg string, args ...interface{}) bool {
	return true
}

func Positive(e interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func Positivef(e interface{}, msg string, args ...interface{}) bool {
	return true
}

func Regexp(rx interface{}, str interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func Regexpf(rx interface{}, str interface{}, msg string, args ...interface{}) bool {
	return true
}

func Same(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func Samef(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	return true
}

func Subset(list interface{}, subset interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func Subsetf(list interface{}, subset interface{}, msg string, args ...interface{}) bool {
	return true
}

func True(value bool, msgAndArgs ...interface{}) bool {
	return true
}

func Truef(value bool, msg string, args ...interface{}) bool {
	return true
}

func WithinDuration(expected time.Time, actual time.Time, delta time.Duration, msgAndArgs ...interface{}) bool {
	return true
}

func WithinDurationf(expected time.Time, actual time.Time, delta time.Duration, msg string, args ...interface{}) bool {
	return true
}

func WithinRange(actual time.Time, start time.Time, end time.Time, msgAndArgs ...interface{}) bool {
	return true
}

func WithinRangef(actual time.Time, start time.Time, end time.Time, msg string, args ...interface{}) bool {
	return true
}

func YAMLEq(expected string, actual string, msgAndArgs ...interface{}) bool {
	return true
}

func YAMLEqf(expected string, actual string, msg string, args ...interface{}) bool {
	return true
}

func Zero(i interface{}, msgAndArgs ...interface{}) bool {
	return true
}

func Zerof(i interface{}, msg string, args ...interface{}) bool {
	return true
}
