# assert

`assert` lets you use `github.com/stretchr/testify/assert` outside of tests.

The idea is to spread assertion code all over your code base while developing
and have the assertions panic, if they fail.

This allows for fast development.

For production code, you compile with

    go build -tags production

and the assertion code will be optimized away.

All methods of `github.com/stretchr/testify/assert#Assertions` are copied as toplevel functions
of this packages. They will just return true, when your program is compiled with the `production` flag and
panic, if an assertion fails otherwise.


## Documentation

For documentation of the functions see https://pkg.go.dev/github.com/stretchr/testify/assert#Assertions

## License

MIT (see LICENSE file)
