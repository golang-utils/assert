//go:build !production
// +build !production

package assert

import (
	"net/http"
	"net/url"
	"time"

	"github.com/stretchr/testify/assert"
)

func Condition(comp assert.Comparison, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Condition(comp, msgAndArgs...)
}

func Conditionf(comp assert.Comparison, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Conditionf(comp, msg, args...)
}

func Contains(s interface{}, contains interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Contains(s, contains, msgAndArgs...)
}

func Containsf(s interface{}, contains interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Containsf(s, contains, msg, args...)
}

func DirExists(path string, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.DirExists(path, msgAndArgs...)
}

func DirExistsf(path string, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.DirExistsf(path, msg, args...)
}

func ElementsMatch(listA interface{}, listB interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.ElementsMatch(listA, listB, msgAndArgs...)
}

func ElementsMatchf(listA interface{}, listB interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.ElementsMatchf(listA, listB, msg, args...)
}

func Empty(object interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Empty(object, msgAndArgs...)
}

func Emptyf(object interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Emptyf(object, msg, args...)
}

func Equal(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Equal(expected, actual, msgAndArgs...)
}

func EqualError(theError error, errString string, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.EqualError(theError, errString, msgAndArgs...)
}

func EqualErrorf(theError error, errString string, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.EqualErrorf(theError, errString, msg, args...)
}

func EqualExportedValues(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.EqualExportedValues(expected, actual, msgAndArgs...)
}

func EqualExportedValuesf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.EqualExportedValuesf(expected, actual, msg, args...)
}

func EqualValues(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.EqualValues(expected, actual, msgAndArgs...)
}

func EqualValuesf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.EqualValuesf(expected, actual, msg, args...)
}

func Equalf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Equalf(expected, actual, msg, args...)
}

func Error(err error, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Error(err, msgAndArgs...)
}

func ErrorAs(err error, target interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.ErrorAs(err, target, msgAndArgs...)
}

func ErrorAsf(err error, target interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.ErrorAsf(err, target, msg, args...)
}

func ErrorContains(theError error, contains string, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.ErrorContains(theError, contains, msgAndArgs...)
}

func ErrorContainsf(theError error, contains string, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.ErrorContainsf(theError, contains, msg, args...)
}

func ErrorIs(err error, target error, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.ErrorIs(err, target, msgAndArgs...)
}

func ErrorIsf(err error, target error, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.ErrorIsf(err, target, msg, args...)
}

func Errorf(err error, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Errorf(err, msg, args...)
}

func Eventually(condition func() bool, waitFor time.Duration, tick time.Duration, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Eventually(condition, waitFor, tick, msgAndArgs...)
}

func EventuallyWithT(condition func(collect *assert.CollectT), waitFor time.Duration, tick time.Duration, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.EventuallyWithT(condition, waitFor, tick, msgAndArgs...)
}

func EventuallyWithTf(condition func(collect *assert.CollectT), waitFor time.Duration, tick time.Duration, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.EventuallyWithTf(condition, waitFor, tick, msg, args...)
}

func Eventuallyf(condition func() bool, waitFor time.Duration, tick time.Duration, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Eventuallyf(condition, waitFor, tick, msg, args...)
}

func Exactly(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Exactly(expected, actual, msgAndArgs...)
}

func Exactlyf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Exactlyf(expected, actual, msg, args...)
}

func Fail(failureMessage string, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Fail(failureMessage, msgAndArgs...)
}

func FailNow(failureMessage string, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.FailNow(failureMessage, msgAndArgs...)
}

func FailNowf(failureMessage string, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.FailNowf(failureMessage, msg, args...)
}

func Failf(failureMessage string, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Failf(failureMessage, msg, args...)
}

func False(value bool, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.False(value, msgAndArgs...)
}

func Falsef(value bool, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Falsef(value, msg, args...)
}

func FileExists(path string, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.FileExists(path, msgAndArgs...)
}

func FileExistsf(path string, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.FileExistsf(path, msg, args...)
}

func Greater(e1 interface{}, e2 interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Greater(e1, e2, msgAndArgs...)
}

func GreaterOrEqualf(e1 interface{}, e2 interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.GreaterOrEqualf(e1, e2, msg, args...)
}

func Greaterf(e1 interface{}, e2 interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Greaterf(e1, e2, msg, args...)
}

func HTTPBodyContains(handler http.HandlerFunc, method string, url string, values url.Values, str interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPBodyContains(handler, method, url, values, str, msgAndArgs...)
}

func HTTPBodyContainsf(handler http.HandlerFunc, method string, url string, values url.Values, str interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPBodyContainsf(handler, method, url, values, str, msg, args...)
}

func HTTPBodyNotContains(handler http.HandlerFunc, method string, url string, values url.Values, str interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPBodyNotContains(handler, method, url, values, str, msgAndArgs...)
}

func HTTPBodyNotContainsf(handler http.HandlerFunc, method string, url string, values url.Values, str interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPBodyNotContainsf(handler, method, url, values, str, msg, args...)
}

func HTTPError(handler http.HandlerFunc, method string, url string, values url.Values, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPError(handler, method, url, values, msgAndArgs...)
}

func HTTPErrorf(handler http.HandlerFunc, method string, url string, values url.Values, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPErrorf(handler, method, url, values, msg, args...)
}

func HTTPRedirect(handler http.HandlerFunc, method string, url string, values url.Values, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPRedirect(handler, method, url, values, msgAndArgs...)
}

func HTTPRedirectf(handler http.HandlerFunc, method string, url string, values url.Values, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPRedirectf(handler, method, url, values, msg, args...)
}

func HTTPStatusCode(handler http.HandlerFunc, method string, url string, values url.Values, statuscode int, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPStatusCode(handler, method, url, values, statuscode, msgAndArgs...)
}

func HTTPStatusCodef(handler http.HandlerFunc, method string, url string, values url.Values, statuscode int, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPStatusCodef(handler, method, url, values, statuscode, msg, args...)
}

func HTTPSuccess(handler http.HandlerFunc, method string, url string, values url.Values, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPSuccess(handler, method, url, values, msgAndArgs...)
}

func HTTPSuccessf(handler http.HandlerFunc, method string, url string, values url.Values, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.HTTPSuccessf(handler, method, url, values, msg, args...)
}

func Implements(interfaceObject interface{}, object interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Implements(interfaceObject, object, msgAndArgs...)
}

func Implementsf(interfaceObject interface{}, object interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Implementsf(interfaceObject, object, msg, args...)
}

func InDelta(expected interface{}, actual interface{}, delta float64, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.InDelta(expected, actual, delta, msgAndArgs...)
}

func InDeltaMapValues(expected interface{}, actual interface{}, delta float64, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.InDeltaMapValues(expected, actual, delta, msgAndArgs...)
}

func InDeltaMapValuesf(expected interface{}, actual interface{}, delta float64, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.InDeltaMapValuesf(expected, actual, delta, msg, args...)
}

func InDeltaSlice(expected interface{}, actual interface{}, delta float64, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.InDeltaSlice(expected, actual, delta, msgAndArgs...)
}

func InDeltaSlicef(expected interface{}, actual interface{}, delta float64, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.InDeltaSlicef(expected, actual, delta, msg, args...)
}

func InDeltaf(expected interface{}, actual interface{}, delta float64, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.InDeltaf(expected, actual, delta, msg, args...)
}

func InEpsilon(expected interface{}, actual interface{}, epsilon float64, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.InEpsilon(expected, actual, epsilon, msgAndArgs...)
}

func InEpsilonSlice(expected interface{}, actual interface{}, epsilon float64, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.InEpsilonSlice(expected, actual, epsilon, msgAndArgs...)
}

func InEpsilonSlicef(expected interface{}, actual interface{}, epsilon float64, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.InEpsilonSlicef(expected, actual, epsilon, msg, args...)
}

func InEpsilonf(expected interface{}, actual interface{}, epsilon float64, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.InEpsilonf(expected, actual, epsilon, msg, args...)
}

func IsDecreasing(object interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.IsDecreasing(object, msgAndArgs...)
}

func IsDecreasingf(object interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.IsDecreasingf(object, msg, args...)
}

func IsIncreasing(object interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.IsIncreasing(object, msgAndArgs...)
}

func IsIncreasingf(object interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.IsIncreasingf(object, msg, args...)
}

func IsNonDecreasing(object interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.IsNonDecreasing(object, msgAndArgs...)
}

func IsNonDecreasingf(object interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.IsNonDecreasingf(object, msg, args...)
}

func IsNonIncreasing(object interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.IsNonIncreasing(object, msgAndArgs...)
}

func IsNonIncreasingf(object interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.IsNonIncreasingf(object, msg, args...)
}

func IsType(expectedType interface{}, object interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.IsType(expectedType, object, msgAndArgs...)
}

func IsTypef(expectedType interface{}, object interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.IsTypef(expectedType, object, msg, args...)
}

func JSONEq(expected string, actual string, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.JSONEq(expected, actual, msgAndArgs...)
}

func JSONEqf(expected string, actual string, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.JSONEqf(expected, actual, msg, args...)
}

func Len(object interface{}, length int, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Len(object, length, msgAndArgs...)
}

func Lenf(object interface{}, length int, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Lenf(object, length, msg, args...)
}

func Less(e1 interface{}, e2 interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Less(e1, e2, msgAndArgs...)
}

func LessOrEqual(e1 interface{}, e2 interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.LessOrEqual(e1, e2, msgAndArgs...)
}

func LessOrEqualf(e1 interface{}, e2 interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.LessOrEqualf(e1, e2, msg, args...)
}

func Lessf(e1 interface{}, e2 interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Lessf(e1, e2, msg, args...)
}

func Negative(e interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Negative(e, msgAndArgs...)
}

func Negativef(e interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Negativef(e, msg, args...)
}

func Never(condition func() bool, waitFor time.Duration, tick time.Duration, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Never(condition, waitFor, tick, msgAndArgs...)
}

func Neverf(condition func() bool, waitFor time.Duration, tick time.Duration, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Neverf(condition, waitFor, tick, msg, args...)
}

func Nil(object interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Nil(object, msgAndArgs...)
}

func Nilf(object interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Nilf(object, msg, args...)
}

func NoDirExists(path string, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NoDirExists(path, msgAndArgs...)
}

func NoDirExistsf(path string, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NoDirExistsf(path, msg, args...)
}

func NoError(err error, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NoError(err, msgAndArgs...)
}

func NoErrorf(err error, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NoErrorf(err, msg, args...)
}

func NoFileExists(path string, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NoFileExists(path, msgAndArgs...)
}

func NoFileExistsf(path string, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NoFileExistsf(path, msg, args...)
}

func NotContains(s interface{}, contains interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotContains(s, contains, msgAndArgs...)
}

func NotContainsf(s interface{}, contains interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotContainsf(s, contains, msg, args...)
}

func NotEmpty(object interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotEmpty(object, msgAndArgs...)
}

func NotEmptyf(object interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotEmptyf(object, msg, args...)
}

func NotEqual(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotEqual(expected, actual, msgAndArgs...)
}

func NotEqualValues(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotEqualValues(expected, actual, msgAndArgs...)
}

func NotEqualValuesf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotEqualValuesf(expected, actual, msg, args...)
}

func NotEqualf(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotEqualf(expected, actual, msg, args...)
}

func NotErrorIs(err error, target error, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotErrorIs(err, target, msgAndArgs...)
}

func NotErrorIsf(err error, target error, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotErrorIsf(err, target, msg, args...)
}

func NotNil(object interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotNil(object, msgAndArgs...)
}

func NotNilf(object interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotNilf(object, msg, args...)
}

func NotPanics(f assert.PanicTestFunc, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotPanics(f, msgAndArgs...)
}

func NotPanicsf(f assert.PanicTestFunc, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotPanicsf(f, msg, args...)
}

func NotRegexp(rx interface{}, str interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotRegexp(rx, str, msgAndArgs...)
}

func NotRegexpf(rx interface{}, str interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotRegexpf(rx, str, msg, args...)
}

func NotSame(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotSame(expected, actual, msgAndArgs...)
}

func NotSamef(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotSamef(expected, actual, msg, args...)
}

func NotSubset(list interface{}, subset interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotSubset(list, subset, msgAndArgs...)
}

func NotSubsetf(list interface{}, subset interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotSubsetf(list, subset, msg, args...)
}

func NotZero(i interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotZero(i, msgAndArgs...)
}

func NotZerof(i interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.NotZerof(i, msg, args...)
}

func Panics(f assert.PanicTestFunc, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Panics(f, msgAndArgs...)
}

func PanicsWithError(errString string, f assert.PanicTestFunc, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.PanicsWithError(errString, f, msgAndArgs...)
}

func PanicsWithErrorf(errString string, f assert.PanicTestFunc, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.PanicsWithErrorf(errString, f, msg, args...)
}

func PanicsWithValue(expected interface{}, f assert.PanicTestFunc, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.PanicsWithValue(expected, f, msgAndArgs...)
}

func PanicsWithValuef(expected interface{}, f assert.PanicTestFunc, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.PanicsWithValuef(expected, f, msg, args...)
}

func Panicsf(f assert.PanicTestFunc, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Panicsf(f, msg, args...)
}

func Positive(e interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Positive(e, msgAndArgs...)
}

func Positivef(e interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Positivef(e, msg, args...)
}

func Regexp(rx interface{}, str interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Regexp(rx, str, msgAndArgs...)
}

func Regexpf(rx interface{}, str interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Regexpf(rx, str, msg, args...)
}

func Same(expected interface{}, actual interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Same(expected, actual, msgAndArgs...)
}

func Samef(expected interface{}, actual interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Samef(expected, actual, msg, args...)
}

func Subset(list interface{}, subset interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Subset(list, subset, msgAndArgs...)
}

func Subsetf(list interface{}, subset interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Subsetf(list, subset, msg, args...)
}

func True(value bool, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.True(value, msgAndArgs...)
}

func Truef(value bool, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Truef(value, msg, args...)
}

func WithinDuration(expected time.Time, actual time.Time, delta time.Duration, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.WithinDuration(expected, actual, delta, msgAndArgs...)
}

func WithinDurationf(expected time.Time, actual time.Time, delta time.Duration, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.WithinDurationf(expected, actual, delta, msg, args...)
}

func WithinRange(actual time.Time, start time.Time, end time.Time, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.WithinRange(actual, start, end, msgAndArgs...)
}

func WithinRangef(actual time.Time, start time.Time, end time.Time, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.WithinRangef(actual, start, end, msg, args...)
}

func YAMLEq(expected string, actual string, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.YAMLEq(expected, actual, msgAndArgs...)
}

func YAMLEqf(expected string, actual string, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.YAMLEqf(expected, actual, msg, args...)
}

func Zero(i interface{}, msgAndArgs ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Zero(i, msgAndArgs...)
}

func Zerof(i interface{}, msg string, args ...interface{}) bool {
	ass := assert.New(nil)
	return ass.Zerof(i, msg, args...)
}
